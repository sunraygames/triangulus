import {vec3, quat, mat4} from 'gl-matrix'
// import * as mat4 from '@2gis/gl-matrix/mat4'

export default function Object3D() {
  this.children = []
  this.parent = null

  this.visible = true
  this.scale = vec3.fromValues(1, 1, 1)
  this.position = vec3.create()
  this.quaternion = quat.create()

  this.matrix = mat4.create()
  this.matrixWorld = mat4.create()
}

Object3D.prototype.add = function(object) {
  if (object.parent) object.parent.remove(object)

  object.parent = this
  this.children.push(object)
}

Object3D.prototype.remove = function(object) {
  const index = this.children.indexOf(object)

  if (index !== -1) {
    object.parent = null
    this.children.splice(index, 1)
  }
}

Object3D.prototype.clear = function() {
  while (this.children.length > 0) this.remove(this.children[0])
}

Object3D.prototype.matrixLocalUpdate = function() {
  mat4.fromRotationTranslationScale(this.matrix, this.quaternion, this.position, this.scale)
  // this.worldMatrixNeedsUpdate = true
}

Object3D.prototype.matrixWorldUpdate = function() {
  if (this.parent) {
    mat4.mul(this.matrixWorld, this.parent.matrixWorld, this.matrix)
  } else {
    mat4.copy(this.matrixWorld, this.matrix)
  }

  this.children.forEach(child => child.updateWorldMatrix())
  // this.worldMatrixNeedsUpdate = false
}
