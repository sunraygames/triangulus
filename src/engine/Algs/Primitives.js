export function createCube(size, inverse = false) {
  const sh = size * 0.5
  let x = 0
  let y = 0
  let z = 0

  const primitive = inverse ? 
  [
    // side z+
    x + sh, y - sh, z + sh,
    x - sh, y + sh, z + sh,
    x - sh, y - sh, z + sh,

    x + sh, y - sh, z + sh,
    x + sh, y + sh, z + sh,
    x - sh, y + sh, z + sh,

    // side z-
    x - sh, y - sh, z - sh,
    x - sh, y + sh, z - sh,
    x + sh, y - sh, z - sh,

    x - sh, y + sh, z - sh,
    x + sh, y + sh, z - sh,
    x + sh, y - sh, z - sh,

    // side x+
    x + sh, y - sh, z - sh,
    x + sh, y + sh, z - sh,
    x + sh, y - sh, z + sh,

    x + sh, y + sh, z - sh,
    x + sh, y + sh, z + sh,
    x + sh, y - sh, z + sh,

    // side x-
    x - sh, y - sh, z + sh,
    x - sh, y + sh, z - sh,
    x - sh, y - sh, z - sh,

    x - sh, y - sh, z + sh,
    x - sh, y + sh, z + sh,
    x - sh, y + sh, z - sh,

    // side y+
    x - sh, y + sh, z - sh,
    x + sh, y + sh, z - sh,
    x - sh, y + sh, z + sh,

    x + sh, y + sh, z - sh,
    x + sh, y + sh, z + sh,
    x - sh, y + sh, z + sh,

    // side y-
    x - sh, y - sh, z - sh,
    x + sh, y - sh, z - sh,
    x - sh, y - sh, z + sh,

    x + sh, y - sh, z - sh,
    x + sh, y - sh, z + sh,
    x - sh, y - sh, z + sh,
  ]
  :
  [
    // side z+
    x + sh, y - sh, z + sh,
    x - sh, y + sh, z + sh,
    x - sh, y - sh, z + sh,

    x + sh, y - sh, z + sh,
    x + sh, y + sh, z + sh,
    x - sh, y + sh, z + sh,

    // side z-
    x - sh, y - sh, z - sh,
    x - sh, y + sh, z - sh,
    x + sh, y - sh, z - sh,

    x - sh, y + sh, z - sh,
    x + sh, y + sh, z - sh,
    x + sh, y - sh, z - sh,

    // side x+
    x + sh, y - sh, z - sh,
    x + sh, y + sh, z - sh,
    x + sh, y - sh, z + sh,

    x + sh, y + sh, z - sh,
    x + sh, y + sh, z + sh,
    x + sh, y - sh, z + sh,

    // side x-
    x - sh, y - sh, z + sh,
    x - sh, y + sh, z - sh,
    x - sh, y - sh, z - sh,

    x - sh, y - sh, z + sh,
    x - sh, y + sh, z + sh,
    x - sh, y + sh, z - sh,

    // side y+
    x - sh, y + sh, z + sh,
    x + sh, y + sh, z - sh,
    x - sh, y + sh, z - sh,

    x - sh, y + sh, z + sh,
    x + sh, y + sh, z + sh,
    x + sh, y + sh, z - sh,

    // side y-
    x - sh, y - sh, z - sh,
    x + sh, y - sh, z - sh,
    x - sh, y - sh, z + sh,

    x + sh, y - sh, z - sh,
    x + sh, y - sh, z + sh,
    x - sh, y - sh, z + sh,
  ]

  return primitive
}

export function createTexture(gl) {
  const texture = gl.createTexture()
  gl.bindTexture(gl.TEXTURE_2D, texture)

  // Так как изображение будет загружено из интернета, может потребоваться время для полной загрузки.
  // Поэтому сначала мы помещаем в текстуру единственный пиксель, чтобы
  // её можно было использовать сразу. После завершения загрузки изображения мы обновим текстуру.
  const level = 0
  const internalFormat = gl.RGBA
  const border = 0
  const srcFormat = gl.RGBA
  const srcType = gl.UNSIGNED_BYTE

  const width = 100
  const height = 100

  const pixelsData = []
  pixelsData.length = width * height * 4
  const stepW = 255 / width
  const stepH = 255 / height
  for (let h = 0; h < height; h++) {
    for (let w = 0; w < width; w++) {
      const os = (h * width + w) * 4
      // pixelsData[os] = 0
      pixelsData[os] = w * stepW
      // pixelsData[os] = 255
      pixelsData[os + 1] = 0
      // pixelsData[os + 1] = w * stepW
      // pixelsData[os + 2] = 0
      pixelsData[os + 2] = h * stepH
      pixelsData[os + 3] = 255
    }
  }
  const pixels = new Uint8Array(pixelsData)
  console.log('pixels=', pixels)

  gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, width, height, border, srcFormat, srcType, pixels)

  // set the filtering so we don't need mips
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
}

// Инициализация текстуры и загрузка изображения.
// Когда загрузка изображения завершена - копируем его в текстуру.
export function loadTexture(gl, url) {
  const texture = gl.createTexture()
  gl.bindTexture(gl.TEXTURE_2D, texture)

  // Так как изображение будет загружено из интернета, может потребоваться время для полной загрузки.
  // Поэтому сначала мы помещаем в текстуру единственный пиксель, чтобы
  // её можно было использовать сразу. После завершения загрузки изображения мы обновим текстуру.
  const level = 0
  const internalFormat = gl.RGBA
  const border = 0
  const srcFormat = gl.RGBA
  const srcType = gl.UNSIGNED_BYTE

  const width = 1
  const height = 1
  const pixel = new Uint8Array([0, 0, 255, 255])  // непрозрачный синий
  gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, width, height, border, srcFormat, srcType, pixel)

  const image = new Image()
  image.onload = () => {
    console.log('texture loaded=', url, image)
    gl.bindTexture(gl.TEXTURE_2D, texture)
    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType, image)

    // У WebGL1 иные требования к изображениям, имеющим размер степени 2,
    // и к не имеющим размер степени 2, поэтому проверяем, что изображение
    // имеет размер степени 2 в обеих измерениях.
    if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
      // Размер соответствует степени 2. Создаём MIP'ы.
      gl.generateMipmap(gl.TEXTURE_2D)
    } else {
      // Размер не соответствует степени 2.
      // Отключаем MIP'ы и устанавливаем натяжение по краям
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
    }
  }
  image.src = url

  return texture
}

export function isPowerOf2(value) {
  return (value & (value - 1)) === 0
}