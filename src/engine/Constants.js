
export const TargetType = {}

export const BufferType = {}

export const UsageType = {}

export const DataType = {}

export const DrawMode = {}

export const MathConstants = {
  DegToRad: Math.PI / 180,
  RadToDeg: Math.PI * 180
}

export function initConstants(gl) {
  BufferType.ARRAY_BUFFER = gl.ARRAY_BUFFER
  BufferType.ELEMENT_ARRAY_BUFFER = gl.ELEMENT_ARRAY_BUFFER

  UsageType.STATIC_DRAW = gl.STATIC_DRAW
  UsageType.DYNAMIC_DRAW = gl.DYNAMIC_DRAW

  DrawMode.TRIANGLES = gl.TRIANGLES

  DataType.FLOAT = gl.FLOAT

  TargetType.TEXTURE_2D = gl.TEXTURE_2D
}

export const deletedVertex = -Infinity
// export const deletedVertex = 0
