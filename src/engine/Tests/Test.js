import {mat4} from 'gl-matrix'
import Stats from 'stats-js'
import dat from 'dat.gui'

// import vertexDefault from './Shaders/vertexDefault'
// import fragmentDefault from './Shaders/fragmentDefault'
import vertexColors from '../Shaders/vertexColors'
import fragmentColors from '../Shaders/fragmentColors'

import Mesh from '../Mesh'
import {UniformMatrix4} from '@/engine/Uniform'
import Material from '../Material'
import Geometry from '../Geometry'
import Buffer from '../Buffer'
import PerspectiveCamera from '../Cameras/PerspectiveCamera'
// import {createOrbitCamera} from '../Cameras/Controls/Orbit'
import {OrbitControls} from '../Cameras/Controls/OrbitThree'
import Scene from '../Scene'
import Renderer from '../Renderer'
import Entity from '../Entity'
import {MathConstants, deletedVertex} from '../Constants'

export function testScene(canvas, container) {
  // if (!window.sdfsdf) return initRenderer(canvas)

  const stats = new Stats()
  // console.log('stats=', stats)
  document.body.appendChild(stats.dom)

  const settings = {
    size: 10,
    count: 10000,
    // count: 27,
    meshPerEntity: false,
    modification: false,
    removing: false,
    primitives: 1
    /* rotationCamera: true,
    rotationMesh: true,
    cameraOffset: 900 */
  }

  const renderer = new Renderer({canvas, webgl: 'webgl2'})

  const scene = new Scene()
  const camera = new PerspectiveCamera(45 * MathConstants.DegToRad, container.clientWidth / container.clientHeight, 0.1, 10000)
  camera.updateProjectionMatrix()
  //camera.controller = createOrbitCamera()

  const controls = new OrbitControls(camera, renderer._canvas)
  controls.listenToKeyEvents(window) // optional
  // controls.addEventListener('change', render) // call this only in static scenes (i.e., if there is no animation loop)
  // controls.enableDamping = true // an animation loop is required when either damping or auto-rotation are enabled
  controls.dampingFactor = 0.05
  controls.screenSpacePanning = false
  controls.minDistance = 10
  controls.maxDistance = 5000
  // controls.minPolarAngle = 0
  controls.maxPolarAngle = Math.PI / 2
  // controls.maxPolarAngle = Math.PI

  console.log('controls=', controls, 'camera=', camera)
  controls.object.position.set(0, 0, -1000)

  // camera.modelViewMatrix = camera.controller.view()
  // mat4.translate(camera.modelViewMatrix, camera.modelViewMatrix,[-0.0, 0.0, -600.0])

  let entities = fillScene(scene, settings)
  console.log('scene=', scene, 'entities=', entities)

  renderer.setSize(container.clientWidth, container.clientHeight)
  container.addEventListener('resize', function() {
    console.log('resize: w=' + container.clientWidth + ', h=' + container.clientHeight)
    // stats.reset()
    camera.aspect = container.clientWidth / container.clientHeight
    camera.updateProjectionMatrix()
    renderer.setSize(container.clientWidth, container.clientHeight)
  })

  // let lastUpdateTime = Date.now()
  function render() {
    requestAnimationFrame(render)

    // const dt = Date.now() - lastUpdateTime
    // console.log('dt=' + dt + ', performance=' + performance.now())
    const time = performance.now()
    const delta = Math.sin(time * 0.001)

    stats.begin()
    /* if (mesh) {
      rotateMesh(dt)
      rotateCamera(dt)
    } */
    if (settings.modification) {
      const count = scene.children.length
      for (let i = 0; i < count; i++) {
        const pb = scene.children[i].geometry.buffers['position']
        const len = pb._array.length
        for (let j = 0; j < len; j++) {
          // pb._array[j] += Math.sin(dt)
          pb._array[j] += delta
        }
        pb.updateFlag = true
        if (settings.primitives === 1) {
          pb.updateRange = {offset: 3 * 36 * 21 * pb.itemSize, count: 21 * 36 * 21 * pb.itemSize} // 36 vertices per cube, 21 cubes in row
        } else pb.updateRange = {offset: 1000 * pb.itemSize, count: 1000 * pb.itemSize}
      }
    }
    if (settings.removing) {
      if (settings._removingCounter === undefined) settings._removingCounter = 0
      if (settings._removingItem === undefined) settings._removingItem = 0

      if (settings._removingCounter === 30) {
        settings._removingCounter = 0

        const itemsPerDim = Math.floor(Math.cbrt(settings.count))
        let x = Math.floor(Math.random() * itemsPerDim)
        let y = Math.floor(Math.random() * itemsPerDim)
        let z = Math.floor(Math.random() * itemsPerDim)
        const removeCount = Math.floor(settings.count * 0.01)
        // const removeCount = 1
        // console.log('REMOVE: x=' + x + ', y=' + y + ', z=' + z + ', itemsPerDim=' + itemsPerDim + ', removingItem=' + settings._removingItem)

        const count = scene.children.length
        for (let i = 0; i < count; i++) {
          const pb = scene.children[i].geometry.buffers['position']
          if (settings.primitives === 1) {
            // const offset = 36 * pb.itemSize * settings._removingItem
            // const count = 36 * pb.itemSize
            const offset = 36 * pb.itemSize * (z * itemsPerDim + y * itemsPerDim + x)
            const count = 36 * pb.itemSize * removeCount
            for (let j = 0; j < count; j++) pb._array[j + offset] = deletedVertex
            pb.updateFlag = true
            pb.updateRange = {offset, count}
            // console.log('updateRange: offset=' + pb.updateRange.offset + ', count=' + pb.updateRange.count)
          }
        }

        settings._removingItem++
        if (settings._removingItem >= settings.count) settings._removingItem = 0
      }

      settings._removingCounter++
    }
    controls.update() // only required if controls.enableDamping = true, or if controls.autoRotate = true
    camera.modelViewMatrix.set(controls.object.matrixWorldInverse.elements)

    renderer.render(scene, camera)
    stats.end()

    // lastUpdateTime = Date.now()
  }

  render()
  // stats.reset()

  // dat gui settings
  const gui = new dat.GUI()
  const countGui = gui.add(settings, 'count', 1, 1000000)
  const sizeGui = gui.add(settings, 'size', 1, 100)
  const meshPerEntityGui = gui.add(settings, 'meshPerEntity')
  gui.add(settings, 'modification')
  gui.add(settings, 'removing')
  let timeout

  function onChange() {
    clearTimeout(timeout)
    timeout = setTimeout(() => {
      entities = fillScene(scene, settings)
      // mesh.material.setTexture(texture)
      // stats.reset()
    }, 1000)
  }

  countGui.onChange(onChange)
  sizeGui.onChange(onChange)
  meshPerEntityGui.onChange(onChange)

  /* gui.add(settings, 'rotationMesh')
  gui.add(settings, 'rotationCamera')

  const guiCameraOffset = gui.add(settings, 'cameraOffset', 0, 10000)
  guiCameraOffset.onChange(function(value) {
    camera.position[2] = value
    // stats.reset()
  }) */
}

function fillScene(scene, {count, size, meshPerEntity}) {
  scene.clear()

  const material = new Material({vertexShader: vertexColors, fragmentShader: fragmentColors}) //vertexShader: vertexDefault, fragmentShader: fragmentDefault
  material.uniforms.projectionMatrix = new UniformMatrix4()
  material.uniforms.modelViewMatrix = new UniformMatrix4()

  const entities = []
  const vertices = []
  const colors = []
  // const size2 = Math.pow(count, 1 / 3)

  let j
  for (let i = 0; i < count; i++) {
     // const primitive = createTriangle(size, size)
    const primitive = createCube(i, count, /*size2 * */size)
    const randomColor = getRandomRGB()

    Array.prototype.push.apply(vertices, primitive)

    for (j = 0; j < primitive.length / 3; j++) Array.prototype.push.apply(colors, randomColor)

    const entity = new Entity()
    entity.vertices = primitive
    // entity.vertices = primitive
    entities.push(entity)

    if (meshPerEntity) {
      const geometry = new Geometry()
      const vertexBuffer = new Buffer(new Float32Array(vertices), 3)
      const colorBuffer = new Buffer(new Float32Array(colors), 3)

      geometry.setBuffer('position', vertexBuffer)
      geometry.setBuffer('color', colorBuffer)

      // geometry.computeNormals()
      geometry.drawRange.count = vertexBuffer.count

      const mesh = new Mesh(geometry, material)
      scene.add(mesh)

      vertices.splice(0)
      colors.splice(0)
    }
  }

  if (!meshPerEntity) {
    const geometry = new Geometry()
    const vertexBuffer = new Buffer(new Float32Array(vertices), 3)
    const colorBuffer = new Buffer(new Float32Array(colors), 3)

    geometry.setBuffer('position', vertexBuffer)
    geometry.setBuffer('color', colorBuffer)

    // geometry.computeNormals()
    geometry.drawRange.count = vertexBuffer.count

    const mesh = new Mesh(geometry, material)
    scene.add(mesh)
  }

  return entities
}

function getRandomRGB() {
  return [Math.random(), Math.random(), Math.random()]
}

export function createCube(i, count, size) {
  const sh = size * 0.1 * 0.5
  const itemsPerDim = Math.floor(Math.cbrt(count))
  let x = i % itemsPerDim
  let y = Math.floor(i / itemsPerDim) % itemsPerDim
  let z = Math.floor(i / (itemsPerDim * itemsPerDim)) % itemsPerDim
  /* const itemsPerDim = Math.floor(Math.sqrt(count))
  let x = i % itemsPerDim
  let y = Math.floor(i / itemsPerDim) % itemsPerDim
  let z = 0 */
  // console.log('x=' + x + ', y=' + y + ', z=' + z + ', itemsPerDim=' + itemsPerDim)
  x = x * size - itemsPerDim * 0.5 * size
  y = y * size - itemsPerDim * 0.5 * size
  z = z * size - itemsPerDim * 0.5 * size

  const primitive = [
    // side z+
    x + sh, y - sh, z + sh,
    x - sh, y + sh, z + sh,
    x - sh, y - sh, z + sh,

    x + sh, y - sh, z + sh,
    x + sh, y + sh, z + sh,
    x - sh, y + sh, z + sh,

    // side z-
    x - sh, y - sh, z - sh,
    x - sh, y + sh, z - sh,
    x + sh, y - sh, z - sh,

    x - sh, y + sh, z - sh,
    x + sh, y + sh, z - sh,
    x + sh, y - sh, z - sh,

    // side x+
    x + sh, y - sh, z - sh,
    x + sh, y + sh, z - sh,
    x + sh, y - sh, z + sh,

    x + sh, y + sh, z - sh,
    x + sh, y + sh, z + sh,
    x + sh, y - sh, z + sh,

    // side x-
    x - sh, y - sh, z + sh,
    x - sh, y + sh, z - sh,
    x - sh, y - sh, z - sh,

    x - sh, y - sh, z + sh,
    x - sh, y + sh, z + sh,
    x - sh, y + sh, z - sh,

    // side y+
    x - sh, y + sh, z + sh,
    x + sh, y + sh, z - sh,
    x - sh, y + sh, z - sh,

    x - sh, y + sh, z + sh,
    x + sh, y + sh, z + sh,
    x + sh, y + sh, z - sh,

    // side y-
    x - sh, y - sh, z - sh,
    x + sh, y - sh, z - sh,
    x - sh, y - sh, z + sh,

    x + sh, y - sh, z - sh,
    x + sh, y - sh, z + sh,
    x - sh, y - sh, z + sh,
  ]

  return primitive
}

export function createTriangle(size, trianglesSize) {
  const x = (0.5 - Math.random()) * size
  const y = (0.5 - Math.random()) * size
  const z = (0.5 - Math.random()) * size

  const primitive = [
    Math.random() + x, Math.random() + y, Math.random() + z,
    Math.random() + x, Math.random() + y, Math.random() + z,
    Math.random() + x, Math.random() + y, Math.random() + z
  ]

  const blockSize = trianglesSize

  return primitive.map(function(x) { return x * blockSize })
}

export function initRenderer(canvas) {
  // Initialize the GL context
  const gl = canvas.getContext('webgl2')

  // Only continue if WebGL is available and working
  if (gl === null) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.')
    return
  }

  // const isWebGL2 = capabilities.isWebGL2
  /* eslint-disable no-undef */
  const isWebGL2 = (typeof WebGL2RenderingContext !== 'undefined' && gl instanceof WebGL2RenderingContext) ||
    (typeof WebGL2ComputeRenderingContext !== 'undefined' && gl instanceof WebGL2ComputeRenderingContext)
  console.log('isWebGL2=' + isWebGL2, 'gl=', gl)
  /* eslint-enable no-undef */

  // Set clear color to black, fully opaque
  gl.clearColor(0.0, 1.0, 1.0, 1.0)
  // Clear the color buffer with specified clear color
  gl.clear(gl.COLOR_BUFFER_BIT)

  // create buffers benchmark
  let time0 = performance.now()
  const positionBuffer = gl.createBuffer()
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer)

  const bufferUsage = gl.DYNAMIC_DRAW // gl.STATIC_DRAW

  /*const bufferSize = 1024 * 1024 * 32
  // const blockSize = 1024 * 1024
  // const blocks = 32
  // const blockSize = 32
  // const blocks = 1024 * 1024
  //const blocks = bufferSize * 0.0001220703125 // 0.000244140625
  //const blockSize = bufferSize / blocks
  const blockSize = 1024 * 4
  const blocks = 1024 * 32 / 4*/

  const bufferSize = 1024 * 1024 * 32
  const blockSize = 1024 * 4
  const blocks = 1024 * 32 / 4

  console.log('bufferSize=' + bufferSize + ', blockSize=' + blockSize + ', blocks=' + blocks + ', result=' + (blocks * blockSize) + ', blockSize/blocks=' + (blockSize / blocks))

  const typedBuffer = new Float32Array(bufferSize)
  // console.log('typedBuffer=', typedBuffer)
  /* const positions = [
    -1.0,  1.0,
    1.0,  1.0,
    -1.0, -1.0,
    1.0, -1.0,
  ] */
  // gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), bufferUsage)
  gl.bufferData(gl.ARRAY_BUFFER, typedBuffer, bufferUsage)

  // update buffers benchmark
  time0 = performance.now()
  gl.bufferSubData(gl.ARRAY_BUFFER, 0, typedBuffer)
  console.log('update all time=' + (performance.now() - time0))

  time0 = performance.now()
  const offset = 3 * blockSize
  const count = blockSize
  if (isWebGL2) gl.bufferSubData(gl.ARRAY_BUFFER, offset * typedBuffer.BYTES_PER_ELEMENT, typedBuffer, offset, count)
  else gl.bufferSubData(gl.ARRAY_BUFFER, offset * typedBuffer.BYTES_PER_ELEMENT, typedBuffer.subarray(offset, offset + count))
  console.log('update single block time=' + (performance.now() - time0))

  time0 = performance.now()
  for (let i = 0; i < blocks; i++) {
    const offset = i * blockSize
    const count = blockSize
    if (isWebGL2) gl.bufferSubData(gl.ARRAY_BUFFER, offset * typedBuffer.BYTES_PER_ELEMENT, typedBuffer, offset, count)
    else gl.bufferSubData(gl.ARRAY_BUFFER, offset * typedBuffer.BYTES_PER_ELEMENT, typedBuffer.subarray(offset, offset + count))
  }
  console.log('update per block time=' + (performance.now() - time0))

  const buffersDatas = []
  buffersDatas.length = blocks
  for (let i = 0; i < blocks; i++) {
    const bufferData = {positionBuffer: gl.createBuffer(), typedBuffer: new Float32Array(blockSize)}
    buffersDatas[i] = bufferData
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferData.positionBuffer)
    gl.bufferData(gl.ARRAY_BUFFER, bufferData.typedBuffer, bufferUsage)
  }

  /*time0 = performance.now()
  for (let i = 0; i < blocks; i++) {
    const bufferData = buffersDatas[i]
    const offset = 0
    const count = blockSize
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferData.positionBuffer)
    if (isWebGL2) gl.bufferSubData(gl.ARRAY_BUFFER, offset * bufferData.typedBuffer.BYTES_PER_ELEMENT, bufferData.typedBuffer, offset, count)
    else gl.bufferSubData(gl.ARRAY_BUFFER, offset * bufferData.typedBuffer.BYTES_PER_ELEMENT, bufferData.typedBuffer.subarray(offset, offset + count))
  }
  console.log('update multiple buffers time=' + (performance.now() - time0))

  gl.deleteBuffer(positionBuffer)*/





  // Vertex shader program
  const vsSource = `
    attribute vec3 aVertexPosition;
    attribute vec4 aVertexColor;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    void main() {
      gl_Position = uProjectionMatrix * uModelViewMatrix * vec4(aVertexPosition, 1);
    }
  `
  const fsSource = `
    void main() {
      gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
    }
  `

  // Initialize a shader program, this is where all the lighting
  // for the vertices and so forth is established.
  const shaderProgram = initShaderProgram(gl, vsSource, fsSource)

  // Collect all the info needed to use the shader program.
  // Look up which attribute our shader program is using
  // for aVertexPosition and look up uniform locations.
  const programInfo = {
    program: shaderProgram,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
      vertexColor: gl.getAttribLocation(shaderProgram, 'aVertexColor'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
    },
  }
  console.log('programInfo=', programInfo)

  // Here's where we call the routine that builds all the
  // objects we'll be drawing.
  // const buffers = {position: positionBuffer} // initBuffers(gl)
  const buffers = initBuffers(gl)

  // Draw the scene
  drawScene(gl, programInfo, buffers)

  /*time0 = performance.now()
  gl.drawArrays(gl.TRIANGLES, 0, bufferSize / 2)
  console.log('draw all time=' + (performance.now() - time0))

  time0 = performance.now()
  for (let i = 0; i < blocks; i++) {
    const offset = i * blockSize
    const count = blockSize
    gl.drawArrays(gl.TRIANGLES, offset / 2, count / 2)
  }
  console.log('draw blocks time=' + (performance.now() - time0))*/
}

export function initRenderer2(canvas) {
  // Initialize the GL context
  const gl = canvas.getContext('webgl')

  // Only continue if WebGL is available and working
  if (gl === null) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.')
    return
  }

  // Set clear color to black, fully opaque
  gl.clearColor(0.0, 0.0, 0.0, 1.0)
  // Clear the color buffer with specified clear color
  gl.clear(gl.COLOR_BUFFER_BIT)
}

export function initRenderer3(canvas) {
  const gl = canvas.getContext('webgl')

  // If we don't have a GL context, give up now

  if (!gl) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.')
    return
  }

  // Vertex shader program
  const vsSource = `
    attribute vec3 aVertexPosition;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    void main() {
      gl_Position = uProjectionMatrix * uModelViewMatrix * vec4(aVertexPosition, 0);
    }
  `
  const fsSource = `
    void main() {
      gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
    }
  `

  // Initialize a shader program, this is where all the lighting
  // for the vertices and so forth is established.
  const shaderProgram = initShaderProgram(gl, vsSource, fsSource)

  // Collect all the info needed to use the shader program.
  // Look up which attribute our shader program is using
  // for aVertexPosition and look up uniform locations.
  const programInfo = {
    program: shaderProgram,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
    },
  }

  // Here's where we call the routine that builds all the
  // objects we'll be drawing.
  const buffers = initBuffers(gl)

  // Draw the scene
  drawScene(gl, programInfo, buffers)
}

function drawScene(gl, programInfo, buffers) {
  gl.clearColor(0.0, 0.0, 0.0, 1.0)  // Clear to black, fully opaque
  gl.clearDepth(1.0)                 // Clear everything
  gl.enable(gl.DEPTH_TEST)           // Enable depth testing
  gl.depthFunc(gl.LEQUAL)            // Near things obscure far things

  // Clear the canvas before we start drawing on it.

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

  // Create a perspective matrix, a special matrix that is
  // used to simulate the distortion of perspective in a camera.
  // Our field of view is 45 degrees, with a width/height
  // ratio that matches the display size of the canvas
  // and we only want to see objects between 0.1 units
  // and 100 units away from the camera.

  const fieldOfView = 45 * Math.PI / 180   // in radians
  const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight
  const zNear = 0.1
  const zFar = 100.0
  const projectionMatrix = mat4.create()

  // note: glmatrix.js always has the first argument
  // as the destination to receive the result.
  mat4.perspective(projectionMatrix,
    fieldOfView,
    aspect,
    zNear,
    zFar)

  // Set the drawing position to the "identity" point, which is
  // the center of the scene.
  const modelViewMatrix = mat4.create()

  // Now move the drawing position a bit to where we want to
  // start drawing the square.

  mat4.translate(modelViewMatrix,     // destination matrix
    modelViewMatrix,     // matrix to translate
    [-0.0, 0.0, -6.0])  // amount to translate

  // Tell WebGL how to pull out the positions from the position
  // buffer into the vertexPosition attribute.
  {
    const numComponents = 3  // pull out 2 values per iteration
    const type = gl.FLOAT    // the data in the buffer is 32bit floats
    const normalize = false  // don't normalize
    const stride = 0         // how many bytes to get from one set of values to the next
                              // 0 = use type and numComponents above
    const offset = 0         // how many bytes inside the buffer to start from
    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position)
    gl.vertexAttribPointer(
      programInfo.attribLocations.vertexPosition,
      numComponents,
      type,
      normalize,
      stride,
      offset)
    gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition)
  }

  // Tell WebGL to use our program when drawing

  gl.useProgram(programInfo.program)

  // Set the shader uniforms

  gl.uniformMatrix4fv(
    programInfo.uniformLocations.projectionMatrix,
    false,
    projectionMatrix)
  gl.uniformMatrix4fv(
    programInfo.uniformLocations.modelViewMatrix,
    false,
    modelViewMatrix)

  {
    const offset = 0
    const vertexCount = 4
    gl.drawArrays(gl.TRIANGLE_STRIP, offset, vertexCount)
  }
}

function initBuffers(gl) {
  // Create a buffer for the square's positions.

  const positionBuffer = gl.createBuffer()

  // Select the positionBuffer as the one to apply buffer
  // operations to from here out.

  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer)

  // Now create an array of positions for the square.

  const positions = [
    -1.0,  1.0, 0,
    1.0,  1.0, 0,
    -1.0, -1.0, 0,
    1.0, -1.0, 0,
  ]

  // Now pass the list of positions into WebGL to build the
  // shape. We do this by creating a Float32Array from the
  // JavaScript array, then use it to fill the current buffer.

  gl.bufferData(gl.ARRAY_BUFFER,
    new Float32Array(positions),
    gl.STATIC_DRAW)

  return {
    position: positionBuffer,
  }
}

//
// Initialize a shader program, so WebGL knows how to draw our data
//
function initShaderProgram(gl, vsSource, fsSource) {
  const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource)
  const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource)

  // Create the shader program

  const shaderProgram = gl.createProgram()
  gl.attachShader(shaderProgram, vertexShader)
  gl.attachShader(shaderProgram, fragmentShader)
  gl.linkProgram(shaderProgram)

  // If creating the shader program failed, alert

  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
    alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram))
    return null
  }

  return shaderProgram
}

//
// creates a shader of the given type, uploads the source and
// compiles it.
//
function loadShader(gl, type, source) {
  const shader = gl.createShader(type)

  // Send the source to the shader object

  gl.shaderSource(shader, source)

  // Compile the shader program

  gl.compileShader(shader)

  // See if it compiled successfully

  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader))
    gl.deleteShader(shader)
    return null
  }

  return shader
}
