import {vec3} from 'gl-matrix'
import Stats from 'stats-js'
import dat from 'dat.gui'

import vertexWorld from '../Shaders/vertexWorld'
import fragmentMaterialColors from '../Shaders/fragmentMaterialColors'

import Mesh from '../Mesh'
import Material/*, {setCurrentMaterial}*/ from '../Material'
import {UniformMatrix4, UniformVector3} from '@/engine/Uniform'
import Geometry from '../Geometry'
import Buffer from '../Buffer'
import PerspectiveCamera from '../Cameras/PerspectiveCamera'
import {OrbitControls} from '../Cameras/Controls/OrbitThree'
import Scene from '../Scene'
import Renderer from '../Renderer'
import {MathConstants} from '../Constants'
import {createCube} from '../Algs/Primitives'

export function testScene(canvas, container) {
  const app = {}

  const panel = new dat.GUI()
  const params = {
    RequestRendering: true,
    SingleObjectRendering: false,
  }
  const RequestRenderingUI = panel.add(params, 'RequestRendering').onFinishChange(val => {
    console.log('onFinishChange - RequestRendering=', val)
  })
  RequestRenderingUI.listen()
  const SingleObjectRenderingUI = panel.add(params, 'SingleObjectRendering').onFinishChange(val => {
    console.log('onFinishChange - SingleObjectRendering=', val)
  })
  SingleObjectRenderingUI.listen()

  const renderer = new Renderer({canvas, webgl: 'webgl2', preserveDrawingBuffer: true})

  const scene = new Scene()
  const camera = new PerspectiveCamera(45 * MathConstants.DegToRad, container.clientWidth / container.clientHeight, 0.1, 10000)
  camera.updateProjectionMatrix()
  //camera.controller = createOrbitCamera()

  const controls = new OrbitControls(camera, renderer._canvas)
  controls.listenToKeyEvents(window) // optional
  // controls.addEventListener('change', render) // call this only in static scenes (i.e., if there is no animation loop)
  // controls.enableDamping = true // an animation loop is required when either damping or auto-rotation are enabled
  controls.dampingFactor = 0.05
  controls.screenSpacePanning = false
  controls.minDistance = 1
  controls.maxDistance = 500
  // controls.minPolarAngle = 0
  controls.maxPolarAngle = Math.PI / 2
  // controls.maxPolarAngle = Math.PI
  controls.addEventListener('change', () => params.RequestRendering = true, false)

  console.log('controls=', controls, 'camera=', camera)
  controls.object.position.set(0, 0, -30)

  createContent(scene, app)
  //console.log('scene=', scene, 'entities=', entities)

  const stats = new Stats()
  // console.log('stats=', stats)
  document.body.appendChild(stats.dom)

  renderer.setSize(container.clientWidth, container.clientHeight)
  container.addEventListener('resize', function() {
    console.log('resize: w=' + container.clientWidth + ', h=' + container.clientHeight)
    // stats.reset()
    camera.aspect = container.clientWidth / container.clientHeight
    camera.updateProjectionMatrix()
    renderer.setSize(container.clientWidth, container.clientHeight)
  })

  app.scene = scene
  console.log('app=', app)

  function renderFrame() {
    console.log('renderFrame')
    // const dt = Date.now() - lastUpdateTime
    // console.log('dt=' + dt + ', performance=' + performance.now())
    //const time = performance.now()
    //const delta = Math.sin(time * 0.001)

    stats.begin()

    controls.update() // only required if controls.enableDamping = true, or if controls.autoRotate = true
    camera.modelViewMatrix.set(controls.object.matrixWorldInverse.elements)

    renderer.render(scene, camera)
    stats.end()

    // lastUpdateTime = Date.now()
  }

  function renderSingleObject() {
    console.log('renderSingleObject')

    vec3.set(app.sceneSingleObject.material.uniforms.color.value, 0, 0, 1) // change material
    app.sceneSingleObject.material.uniforms.color.needsUpdate = true

    const gl = renderer._gl
    // ничего не чистим, хотим нарисовать поверх
    /*gl.clearDepth(1)
    gl.clearStencil(0)

    gl.clearColor.apply(gl, this.clearColor)
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)*/




    // scene.render(gl, camera)
    /*gl.enable(gl.DEPTH_TEST)
    gl.depthFunc(gl.LEQUAL)

    gl.frontFace(gl.CCW)
    gl.cullFace(gl.BACK)
    gl.enable(gl.CULL_FACE)
    gl.disable(gl.BLEND)

    setCurrentMaterial(null)*/

    app.sceneSingleObject.render(gl, camera)
    vec3.set(app.sceneSingleObject.material.uniforms.color.value, 1, 1, 0) // restore material
    app.sceneSingleObject.material.uniforms.color.needsUpdate = true
  }

  // let lastUpdateTime = Date.now()
  function render() {
    requestAnimationFrame(render)

    if (params.RequestRendering) {
      params.RequestRendering = false
      renderFrame()
    } else if (params.SingleObjectRendering) {
      params.SingleObjectRendering = false
      renderSingleObject()
    } else {
      // if (this.stats) this.stats.drawPanel.update(-1, 100000)
    }
  }

  render()
  // stats.reset()
}

function createContent(scene, app) {
  const boxes = []
  const size = 0.75

  scene.clear()

  const primitive = createCube(size)
  const vertices = []
  // const colors = []

  // const randomColor = getRandomRGB()
  Array.prototype.push.apply(vertices, primitive)
  // for (let j = 0; j < primitive.length / 3; j++) Array.prototype.push.apply(colors, randomColor)
  // console.log('vertices=', vertices, 'colors=', colors)

  const geometry = new Geometry()
  const vertexBuffer = new Buffer(new Float32Array(vertices), 3)
  // const colorBuffer = new Buffer(new Float32Array(colors), 3)

  geometry.setBuffer('position', vertexBuffer)
  // geometry.setBuffer('color', colorBuffer)

  // geometry.computeNormals()
  geometry.drawRange.count = vertexBuffer.count

  for (let i = 0; i < 10; i++) {
    for (let j = 0; j < 10; j++) {
      const material = new Material({vertexShader: vertexWorld, fragmentShader: fragmentMaterialColors}) //vertexShader: vertexDefault, fragmentShader: fragmentDefault
      material.uniforms.projectionMatrix = new UniformMatrix4()
      material.uniforms.modelViewMatrix = new UniformMatrix4()
      material.uniforms.matrixWorld = new UniformMatrix4()
      material.uniforms.color = new UniformVector3(vec3.fromValues(1, 1, 0))

      const mesh = new Mesh(geometry, material)
      vec3.set(mesh.position, i, j, 0)

      mesh.matrixLocalUpdate()
      mesh.matrixWorldUpdate()

      scene.add(mesh)
      boxes.push(mesh)
    }
  }

  app.boxes = boxes

  app.sceneSingleObject = boxes[5 * 10 + 5/*this.boxes.length * 0.5*/]
}