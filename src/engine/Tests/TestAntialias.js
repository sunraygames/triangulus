// import Stats from 'stats-js'
import {vec3} from 'gl-matrix'
import dat from 'dat.gui'

import Renderer from '../Renderer'
import Scene from '../Scene'
import RenderTarget from '@/engine/RenderTarget'
import Geometry from '@/engine/Geometry'
import Buffer from '@/engine/Buffer'
import Material from '@/engine/Material'
import {Texture2D} from '@/engine/Texture'
import Mesh from '@/engine/Mesh'
import {UniformMatrix4, UniformVector3, UniformInt, UniformFloat, UniformTexture2D} from '@/engine/Uniform'
import PerspectiveCamera from '../Cameras/PerspectiveCamera'
import OrthographicCamera from '../Cameras/OrthographicCamera'
import {OrbitControls} from '../Cameras/Controls/OrbitThree'

import {MathConstants} from '../Constants'

import vertexCircle from '@/engine/Shaders/vertexCircle'
import fragmentCircle from '@/engine/Shaders/fragmentCircle'
import vertex2D from '@/engine/Shaders/vertex2D'
import {getShaderFragment2D} from '@/engine/Shaders/fragment2D'

const f2i = 10000.0

export function testScene(canvas, container) {
  const app = {}

  // const stats = new Stats()
  // document.body.appendChild(stats.dom)

  const renderer = new Renderer({canvas, webgl: 'webgl2', antialias: true})

  const scene = new Scene()
  const camera = new PerspectiveCamera(45 * MathConstants.DegToRad, container.clientWidth / container.clientHeight, 0.1, 10000)
  // const camera = new OrthographicCamera(-container.clientWidth, container.clientWidth, container.clientHeight, -container.clientHeight, 0.1, 10000)
  // const camera = new OrthographicCamera(-1, 1, 1, -1, 0.1, 10000)
  camera.updateProjectionMatrix()

  const controls = new OrbitControls(camera, renderer._canvas)
  controls.listenToKeyEvents(window) // optional
  controls.dampingFactor = 0.05
  controls.screenSpacePanning = false
  controls.minDistance = 1
  controls.maxDistance = 10000
  // controls.minPolarAngle = 0
  // controls.maxPolarAngle = Math.PI / 2
  // controls.maxPolarAngle = Math.PI

  controls.object.position.set(0, 0, 10)
  //controls.enabled = false

  renderer.setSize(container.clientWidth, container.clientHeight)
  container.addEventListener('resize', () => {
    console.log('resize: w=' + container.clientWidth + ', h=' + container.clientHeight)
    camera.aspect = container.clientWidth / container.clientHeight
    camera.updateProjectionMatrix()
    renderer.setSize(container.clientWidth, container.clientHeight)
  })

  createCircle(scene)

  const dataTypes = {combined: -1, tColor: 0, tUv: 1, tDerivatives: 2, tData: 3}

  const scene2D = createScene2D()
  // console.log('scene2D=', scene2D)
  scene2D.mesh.material.uniforms.dataType = new UniformInt(dataTypes.combined)
  scene2D.mesh2.material.uniforms.dataType = new UniformInt(dataTypes.combined)
  scene2D.mesh.material.uniforms.f2i = new UniformFloat(f2i)
  scene2D.mesh2.material.uniforms.f2i = new UniformFloat(f2i)
  /* const tex = loadTexture(gl, 'testPicTestHD.png')
  tex.slot = 2
  scene2D.mesh.material.uniforms.tDiffuseCustom = new UniformTexture2D(tex) */

  // https://webgl2fundamentals.org/webgl/lessons/webgl-render-to-texture.html
  const gl = renderer._gl

  const targetTextureWidth = container.clientWidth, targetTextureHeight = container.clientHeight

  const dataTypesKeys = Object.keys(dataTypes)
  const textures = []
  dataTypesKeys.forEach((dataTypeKey, dataTypeKeyI) => {
    if (dataTypeKeyI === 0) return // skip 0

    const dataTypeValue = dataTypes[dataTypeKey]
    // create to render to
    // const tex = new Texture2D(0, gl.RGBA, targetTextureWidth, targetTextureHeight, 0, gl.RGBA, gl.UNSIGNED_BYTE, null, /*i*/dataTypeValue)
    // const tex = new Texture2D(0, gl.RGBA, targetTextureWidth, targetTextureHeight, 0, gl.RGBA, gl.BYTE, null, /*i*/dataTypeValue)
    // const tex = new Texture2D(0, gl.RGBA32F, targetTextureWidth, targetTextureHeight, 0, gl.RGBA, gl.FLOAT, null, /*i*/dataTypeValue)
    const tex = new Texture2D(0, gl.RGBA32I, targetTextureWidth, targetTextureHeight, 0, gl.RGBA_INTEGER, gl.INT, null, /*i*/dataTypeValue)
    // const tex = new Texture2D(0, gl.RGBA8I, targetTextureWidth, targetTextureHeight, 0, gl.RGBA_INTEGER, gl.INT, null, /*i*/dataTypeValue)
    // const tex = new Texture2D(0, gl.RGBA, targetTextureWidth, targetTextureHeight, 0, gl.RGBA, gl.INT, null, /*i*/dataTypeValue)
    tex.init(gl)
    textures.push(tex)

    scene2D.mesh.material.uniforms[dataTypeKey] = new UniformTexture2D(tex)
    scene2D.mesh2.material.uniforms[dataTypeKey] = new UniformTexture2D(tex)
  })

  const rt = new RenderTarget()
  rt.init(gl, textures)

  const tex = new Texture2D(0, gl.RGBA32I, targetTextureWidth, targetTextureHeight, 0, gl.RGBA_INTEGER, gl.INT, null, 0/*4*/)
  tex.init(gl)
  const rt2 = new RenderTarget()
  rt2.init(gl, [tex])
  // console.log('rt=', rt)

  let captureFrame = true
  const params = {
    showData: scene2D.mesh.material.uniforms.dataType.value,
    data: scene2D.mesh.material.uniforms.dataType.value,
    // dataValue: '',
    // data0: 0, data1: 0, data2: 0, data3: 0,
    data0: '', data1: '', data2: '', data3: '',
  }
  const panel = new dat.GUI({width: 350})
  panel.add(params, 'showData', dataTypes).onFinishChange(val => {
    // console.log('showData changed=', val)
    scene2D.mesh.material.uniforms.dataType.value = val
  })
  panel.add(params, 'data', dataTypes).onFinishChange((/*val*/) => {
    // console.log('data changed=', val)
    captureFrame = true
  })
  // const prec = 0.000001
  // panel.add(params, 'dataValue').listen()
  panel.add(params, 'data0').listen()//.step(prec)
  panel.add(params, 'data1').listen()//.step(prec)
  panel.add(params, 'data2').listen()//.step(prec)
  panel.add(params, 'data3').listen()//.step(prec)

  // let lastUpdateTime = Date.now()
  function render() {
    requestAnimationFrame(render)

    controls.update() // only required if controls.enableDamping = true, or if controls.autoRotate = true
    camera.modelViewMatrix.set(controls.object.matrixWorldInverse.elements)

    gl.bindFramebuffer(gl.FRAMEBUFFER, rt._frameBuffer)
    renderer.render(scene, camera)

    if (captureFrame) {
      captureFrame = false

      const dataTypeOld = scene2D.mesh.material.uniforms.dataType.value
      scene2D.mesh.material.uniforms.dataType.value = params.data
      scene2D.mesh2.material.uniforms.dataType.value = params.data

      /*gl.bindFramebuffer(gl.FRAMEBUFFER, null)
      renderer.render(scene2D.scene, scene2D.camera)

      const pixels = new Uint8Array(gl.drawingBufferWidth * gl.drawingBufferHeight * 4)
      // const pixels = new Int8Array(gl.drawingBufferWidth * gl.drawingBufferHeight * 4)
      gl.readPixels(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight, gl.RGBA, gl.UNSIGNED_BYTE, pixels)
      // gl.readPixels(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight, gl.RGBA, gl.BYTE, pixels)
      // console.log('got pixels=', pixels) // Uint8Array
      app.textureData = {data: pixels, width: gl.drawingBufferWidth, height: gl.drawingBufferHeight, pixelSize: 4}*/

      gl.bindFramebuffer(gl.FRAMEBUFFER, rt2._frameBuffer)
      renderer.render(scene2D.scene2, scene2D.camera)

      const tex0 = rt2.textures[0]
      const pixels = tex0.internalFormat === gl.RGBA32I ? new Int32Array(gl.drawingBufferWidth * gl.drawingBufferHeight * 4) : new Uint8Array(gl.drawingBufferWidth * gl.drawingBufferHeight * 4)
      // const pixels = new Float32Array(gl.drawingBufferWidth * gl.drawingBufferHeight * 4)
      // gl.readPixels(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight, gl.RGBA_INTEGER, gl.INT, pixels)
      gl.readPixels(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight, tex0.format, tex0.type, pixels)
      // gl.readPixels(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight, gl.RGBA, gl.FLOAT, pixels)
      // console.log('got pixels=', pixels) // Uint8Array
      app.textureData = {data: pixels, width: gl.drawingBufferWidth, height: gl.drawingBufferHeight, pixelSize: 4}

      scene2D.mesh.material.uniforms.dataType.value = dataTypeOld
      scene2D.mesh2.material.uniforms.dataType.value = dataTypeOld
    }

    gl.bindFramebuffer(gl.FRAMEBUFFER, null)
    renderer.render(scene2D.scene, scene2D.camera)
    //renderer.render(scene2D.scene2, scene2D.camera)
  }

  // container.addEventListener('pointermove', e => {}, false)

  container.addEventListener('pointerdown', e => {
    // console.log('e=', e)
    if (app.textureData) {
      // const pixelIndex = (e.clientY * app.textureData.width + e.clientX) * app.textureData.pixelSize
      const pixelIndex = ((app.textureData.height - e.clientY) * app.textureData.width + e.clientX) * app.textureData.pixelSize // flip Y
      const pixel = app.textureData.data.slice(pixelIndex, pixelIndex + app.textureData.pixelSize)
      const pixelData = new Float32Array(4)
      for (let i = 0, pl = pixel.length; i < pl; i++) {
        pixelData[i] = pixel[i] / f2i
        params['data' + i] = pixelData[i].toString()
      }
      console.log('pixels=', app.textureData, 'pixelIndex=', pixelIndex, 'pixel=', pixel, 'pixelData=', pixelData, 'x=', e.clientX, 'y=', e.clientY)
      // params.dataValue = pixel.toString()
      // params.dataValue = pixelData.toString()
    }
  }, false)

  /* document.addEventListener('keydown', e => {
    // console.log('keydown=', e)
    if (e.key === 'z') {captureFrame = true}
  }, false) */

  controls.addEventListener('change', () => {captureFrame = true}, false)

  render()
}

export function createScene2D() {
  const camera = new OrthographicCamera(-1, 1, 1, -1, 0, 1)

  // const size = 0.5
  const size = 1
  const vertices = [
    -size, -size,
    size, -size,
    size, size,

    -size, -size,
    size, size,
    -size, size,
  ]

  const uvs = [
    0, 0,
    1, 0,
    1, 1,

    0, 0,
    1, 1,
    0, 1,
  ]

  const geometry = new Geometry()
  const vertexBuffer = new Buffer(new Float32Array(vertices), 2)
  const uvBuffer = new Buffer(new Float32Array(uvs), 2)
  geometry.setBuffer('position', vertexBuffer)
  geometry.setBuffer('uv', uvBuffer)
  geometry.drawRange.count = vertexBuffer.count

  const material = new Material({vertexShader: vertex2D, fragmentShader: getShaderFragment2D(false)})
  material.uniforms.projectionMatrix = new UniformMatrix4()
  // material.uniforms.modelViewMatrix = new UniformMatrix4()
  const mesh = new Mesh(geometry, material)
  // console.log('mesh=', mesh)
  mesh.matrixLocalUpdate()
  mesh.matrixWorldUpdate()
  const scene = new Scene()
  scene.add(mesh)

  const material2 = new Material({vertexShader: vertex2D, fragmentShader: getShaderFragment2D(true)})
  material2.uniforms.projectionMatrix = new UniformMatrix4()
  // material2.uniforms.modelViewMatrix = new UniformMatrix4()
  const mesh2 = new Mesh(geometry, material2)
  // console.log('mesh=', mesh)
  mesh2.matrixLocalUpdate()
  mesh2.matrixWorldUpdate()
  const scene2 = new Scene()
  scene2.add(mesh2)

  return {camera, scene, mesh, scene2, mesh2}
}

function createCircle(scene) {
  scene.clear()

  const size = 1

  const vertices = [
    -size, -size, 0,
    size, -size, 0,
    size, size, 0,

    -size, -size, 0,
    size, size, 0,
    -size, size, 0,
  ]

  const uvs = [
    -1, -1,
    1, -1,
    1, 1,

    -1, -1,
    1, 1,
    -1, 1,
  ]

  const geometry = new Geometry()
  const vertexBuffer = new Buffer(new Float32Array(vertices), 3)
  const uvBuffer = new Buffer(new Float32Array(uvs), 2)
  geometry.setBuffer('position', vertexBuffer)
  geometry.setBuffer('uv', uvBuffer)
  geometry.drawRange.count = vertexBuffer.count

  const material = new Material({vertexShader: vertexCircle, fragmentShader: fragmentCircle})
  material.uniforms.projectionMatrix = new UniformMatrix4()
  material.uniforms.modelViewMatrix = new UniformMatrix4()
  material.uniforms.matrixWorld = new UniformMatrix4()
  material.uniforms.color = new UniformVector3(vec3.fromValues(0, 0, 1))
  material.uniforms.f2i = new UniformFloat(f2i)
  // material.v = 3

  const mesh = new Mesh(geometry, material)

  mesh.matrixLocalUpdate()
  mesh.matrixWorldUpdate()

  scene.add(mesh)
}