import {vec3} from 'gl-matrix'
import Stats from 'stats-js'
//import dat from 'dat.gui'

import vertexWorld from '../Shaders/vertexWorld'
import fragmentMaterialColors from '../Shaders/fragmentMaterialColors'

import Mesh from '../Mesh'
import Material, {setCurrentMaterial} from '../Material'
import {UniformMatrix4, UniformVector3} from '@/engine/Uniform'
import Geometry from '../Geometry'
import Buffer from '../Buffer'
import PerspectiveCamera from '../Cameras/PerspectiveCamera'
import {OrbitControls} from '../Cameras/Controls/OrbitThree'
import Scene from '../Scene'
import Renderer from '../Renderer'
import {MathConstants} from '../Constants'
import {createCube} from '../Algs/Primitives'

//import {Group, Vector3, DoubleSide, MeshPhongMaterial, Mesh} from 'three'
import * as THREE from 'three'
import {STLLoader} from 'three/examples/jsm/loaders/STLLoader'

/*eslint no-unused-vars: "off"*/

export function testScene(canvas, container) {
  const app = {}

  //const panel = new dat.GUI()
  //const params = {}

  const renderer = new Renderer({canvas, webgl: 'webgl2', stencil: true})

  const scene = new Scene()
  const camera = new PerspectiveCamera(45 * MathConstants.DegToRad, container.clientWidth / container.clientHeight, 0.1, 10000)
  camera.updateProjectionMatrix()
  //camera.controller = createOrbitCamera()

  const controls = new OrbitControls(camera, renderer._canvas)
  controls.listenToKeyEvents(window) // optional
  // controls.addEventListener('change', render) // call this only in static scenes (i.e., if there is no animation loop)
  // controls.enableDamping = true // an animation loop is required when either damping or auto-rotation are enabled
  controls.dampingFactor = 0.05
  controls.screenSpacePanning = false
  controls.minDistance = 1
  controls.maxDistance = 500
  // controls.minPolarAngle = 0
  controls.maxPolarAngle = Math.PI / 2
  // controls.maxPolarAngle = Math.PI

  console.log('controls=', controls, 'camera=', camera)
  controls.object.position.set(0, 0, -30)

  createContent(scene, app)
  //console.log('scene=', scene, 'entities=', entities)
  createContentThree(scene, app)

  const stats = new Stats()
  // console.log('stats=', stats)
  document.body.appendChild(stats.dom)

  renderer.setSize(container.clientWidth, container.clientHeight)
  container.addEventListener('resize', function() {
    console.log('resize: w=' + container.clientWidth + ', h=' + container.clientHeight)
    // stats.reset()
    camera.aspect = container.clientWidth / container.clientHeight
    camera.updateProjectionMatrix()
    renderer.setSize(container.clientWidth, container.clientHeight)
  })

  app.scene = scene
  console.log('app=', app)

  const setBackPass = (glState, isBlend) => {
    const {gl} = glState
    glState.setBlend(isBlend)
    glState.setDepthTest(true)
    glState.setDepthMask(true)
    glState.setCullFace(glState.Cull.front)
    glState.setStencilTest(true)
    glState.setStencilFunc(gl.ALWAYS, 0, 0)
    glState.setStencilMask(0xffffffff)
    glState.setStencilOp(gl.KEEP, gl.INCR, gl.INCR)

    /*gl.enable(gl.BLEND)
    gl.enable(gl.DEPTH_TEST)
    gl.depthMask(true)
    gl.enable(gl.STENCIL_TEST)

    gl.enable(gl.CULL_FACE)
    gl.cullFace(gl.FRONT)

    gl.stencilFunc(gl.ALWAYS, 0, 0)
    gl.stencilMask(0xffffffff)
    gl.stencilOp(gl.KEEP, gl.INCR, gl.INCR)*/
  }

  /* global setFrontPass */
  const setFrontPass = (glState, isBlend) => {
    const {gl} = glState
    glState.setBlend(isBlend)
    glState.setDepthMask(false)
    glState.setCullFace(glState.Cull.back)
    glState.setStencilFunc(gl.ALWAYS, 0, 0x0)
    glState.setStencilMask(0xffffffff)
    glState.setStencilOp(gl.KEEP, gl.DECR, gl.KEEP)

    /*gl.enable(gl.BLEND)
    gl.depthMask(false)

    gl.enable(gl.CULL_FACE)
    gl.cullFace(gl.BACK)

    gl.stencilFunc(gl.ALWAYS, 0, 0)
    gl.stencilMask(0xffffffff)
    gl.stencilOp(gl.KEEP, gl.DECR, gl.KEEP)*/
  }

  /* global setIntersectionPass */
  const setIntersectionPass = (glState, isBlend) => {
    const {gl} = glState
    glState.setBlend(isBlend)
    glState.setDepthMask(true)
    glState.setCullFace(glState.Cull.back)
    glState.setStencilFunc(gl.LESS, 0x01, 0xffffffff)
    glState.setStencilMask(0xffffffff)
    glState.setStencilOp(gl.KEEP, gl.KEEP, gl.KEEP)

    /*gl.enable(gl.BLEND)
    gl.depthMask(true)

    gl.enable(gl.CULL_FACE)
    gl.cullFace(gl.BACK)

    gl.stencilFunc(gl.LESS, 0x01, 0xffffffff)
    gl.stencilMask(0xffffffff)
    gl.stencilOp(gl.KEEP, gl.KEEP, gl.KEEP)*/
  }

  const drawScene = (gl, scene, camera) => {
    //renderer.render(scene, camera)
    setCurrentMaterial(null)

    const count = scene.children.length
    for (let i = 0; i < count; i++) scene.children[i].render(gl, camera)
  }

  app.boxes.forEach(box => box.material._colorStored = vec3.clone(box.material.uniforms.color.value))

  const gl = renderer._gl
  const glState = new GlState(gl)

  /*this.setDepthTest(true)
  this.setDepthMask(true)
  this.setStencilTest(false)
  this.setBlend(false)
  this.clearColor = 0x0
  this.stacks = {
    framebuffer: [],
    renderbuffer: [],
  }*/
  /*gl.enable(gl.DEPTH_TEST)
  gl.depthMask(true)
  gl.disable(gl.STENCIL_TEST)
  gl.disable(gl.BLEND)

  gl.clearColor(0, 0, 0, 1)
  gl.clearDepth(1.0)
  gl.clearStencil(0)
  gl.blendEquationSeparate(gl.FUNC_ADD, gl.FUNC_ADD)
  gl.blendFuncSeparate(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA, gl.ONE, gl.ONE_MINUS_SRC_ALPHA)
  gl.depthFunc(gl.LEQUAL)*/

  const renderFrame = () => {
    // const dt = Date.now() - lastUpdateTime
    // console.log('dt=' + dt + ', performance=' + performance.now())
    //const time = performance.now()
    //const delta = Math.sin(time * 0.001)

    stats.begin()

    controls.update() // only required if controls.enableDamping = true, or if controls.autoRotate = true
    camera.modelViewMatrix.set(controls.object.matrixWorldInverse.elements)

    // renderer.render(scene, camera)

    //gl.clearDepth(1)
    //gl.clearStencil(0)
    //gl.clearColor.apply(gl, renderer.clearColor)
    //gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT | gl.STENCIL_BUFFER_BIT)
    glState.clear()

    const isBlend = false

    setBackPass(glState, isBlend)
    app.boxes.forEach(box => {
      vec3.copy(box.material.uniforms.color.value, box.material._colorStored)
      //vec3.set(box.material.uniforms.color.value, 1, 1, 0)
      box.material.uniforms.color.needsUpdate = true
    })
    drawScene(gl, scene, camera)

    setFrontPass(glState, isBlend)
    //args.scene.lights[0].irradiance = [0, 1, 1, flatColorAlpha]
    app.boxes.forEach(box => {
      vec3.set(box.material.uniforms.color.value, 0, 1, 1)
      box.material.uniforms.color.needsUpdate = true
    })
    drawScene(gl, scene, camera)

    setIntersectionPass(glState, isBlend)
    //args.scene.lights[0].irradiance = [1, 0, 0, alpha]
    app.boxes.forEach(box => {
      vec3.set(box.material.uniforms.color.value, 1, 0, 0)
      //vec3.set(box.material.uniforms.color.value, Math.random(), 0, 0)
      box.material.uniforms.color.needsUpdate = true
    })
    drawScene(gl, scene, camera)

    stats.end()

    // lastUpdateTime = Date.now()
  }

  // let lastUpdateTime = Date.now()
  const render = () => {
    requestAnimationFrame(render)
  
    renderFrame()
  }

  render()
  // stats.reset()
}

const createContent = (scene, app) => {
  const boxes = []
  const size = 0.75

  scene.clear()

  for (let i = 0; i < 2; i++) {
    const primitive = createCube(size/*, i === 0*/)
    const vertices = []
    // const colors = []

    // const randomColor = getRandomRGB()
    Array.prototype.push.apply(vertices, primitive)
    // for (let j = 0; j < primitive.length / 3; j++) Array.prototype.push.apply(colors, randomColor)
    // console.log('vertices=', vertices, 'colors=', colors)

    const geometry = new Geometry()
    const vertexBuffer = new Buffer(new Float32Array(vertices), 3)
    // const colorBuffer = new Buffer(new Float32Array(colors), 3)

    geometry.setBuffer('position', vertexBuffer)
    // geometry.setBuffer('color', colorBuffer)

    // geometry.computeNormals()
    geometry.drawRange.count = vertexBuffer.count

    const material = new Material({vertexShader: vertexWorld, fragmentShader: fragmentMaterialColors}) //vertexShader: vertexDefault, fragmentShader: fragmentDefault
    material.uniforms.projectionMatrix = new UniformMatrix4()
    material.uniforms.modelViewMatrix = new UniformMatrix4()
    material.uniforms.matrixWorld = new UniformMatrix4()
    material.uniforms.color = new UniformVector3(vec3.fromValues(1, 1, 0))

    const mesh = new Mesh(geometry, material)
    vec3.set(mesh.position, 0, 0, 0)

    mesh.matrixLocalUpdate()
    mesh.matrixWorldUpdate()

    scene.add(mesh)
    boxes.push(mesh)
  }

  vec3.set(boxes[0].position, 0.3, 0.3, 0.3)
  boxes[0].matrixLocalUpdate()
  boxes[0].matrixWorldUpdate()
  vec3.set(boxes[0].material.uniforms.color.value, 0, 0, 1)

  app.boxes = boxes
}

const createContentThree = async (scene, app) => {
  //const boxes = []

  const stlLoader = new STLLoader()
  const paths = ['./content/IOS superimposition/ios_mesh_aligned_upper.stl', './content/IOS superimposition/ios_mesh_aligned_lower.stl']

  const loadToGroup = async (paths/*, group*/) => {
    await Promise.all(
      paths.map((path) =>
          new Promise((resolve) => {
            const g = stlLoader.load(path, g => {
              // I do that thing to make transform controls works in local space coords
              g.computeBoundingBox()
              const center = new THREE.Vector3()
              g.boundingBox?.getCenter(center)
              // why do I do that?
              g.translate(-center?.x, -center?.y, -center.z)

              /*const m = new THREE.Mesh(
                g,
                new THREE.MeshPhongMaterial({
                  side: THREE.DoubleSide,
                  transparent: true,
                  opacity: 0.3,
                })
              )
              m.position.copy(center)
              m.name = path
              group.add(m)*/

              // to triangulus scene
              //m.updateMatrixWorld(true)
              //g.applyMatrix4(m.matrixWorld)
              const geometry = new Geometry()
              const vertexBuffer = new Buffer(new Float32Array(g.attributes.position.array), 3)
              // const colorBuffer = new Buffer(new Float32Array(colors), 3)

              geometry.setBuffer('position', vertexBuffer)
              // geometry.setBuffer('color', colorBuffer)

              // geometry.computeNormals()
              geometry.drawRange.count = vertexBuffer.count

              const material = new Material({vertexShader: vertexWorld, fragmentShader: fragmentMaterialColors}) //vertexShader: vertexDefault, fragmentShader: fragmentDefault
              material.uniforms.projectionMatrix = new UniformMatrix4()
              material.uniforms.modelViewMatrix = new UniformMatrix4()
              material.uniforms.matrixWorld = new UniformMatrix4()
              material.uniforms.color = new UniformVector3(vec3.fromValues(1, 1, 0))

              const mesh = new Mesh(geometry, material)
              vec3.set(mesh.position, center.x, center.y, center.z)

              scene.add(mesh)
              //boxes.push(mesh)
              app.boxes.push(mesh)

              const pathIndex = paths.indexOf(path)
              console.log('mesh=', mesh, 'pathIndex=', pathIndex)
              if (pathIndex === 0) {
                vec3.set(mesh.material.uniforms.color.value, 0, 0, 1)
                mesh.position[2] -= 1
              }
              mesh.material._colorStored = vec3.clone(mesh.material.uniforms.color.value)

              mesh.matrixLocalUpdate()
              mesh.matrixWorldUpdate()

              resolve()
            })
        })
      )
    )
    //return group
  }

  //const group = new THREE.Group()
  //group.name = 'teeth'
  console.log('paths=', paths)
  return await loadToGroup(paths/*, group*/)
}

//import VMath from './math.js'

class GlState {
  constructor(gl) {
    this.gl = gl
    this.setDepthTest(true)
    this.setDepthMask(true)
    this.setStencilTest(false)
    this.setBlend(false)
    this.clearColor = 0x0
    this.stacks = {
      framebuffer: [],
      renderbuffer: [],
    }
    gl.clearColor(0, 0, 0, 1)
    gl.clearDepth(1.0)
    gl.clearStencil(0)
    gl.blendEquationSeparate(gl.FUNC_ADD, gl.FUNC_ADD)
    gl.blendFuncSeparate(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA, gl.ONE, gl.ONE_MINUS_SRC_ALPHA)
    gl.depthFunc(gl.LEQUAL)
    this.Cull = {
      none: gl.NONE,
      front: gl.FRONT,
      back: gl.BACK,
      both: gl.FRONT_AND_BACK,
    }
    this.cullFace = this.Cull.none
    this.stencilOp = {
      fail: gl.KEEP,
      zfail: gl.KEEP,
      zpass: gl.KEEP,
    }
    this.stencilFunc = {
      func: gl.ALWAYS,
      ref: 0,
      // read mask
      mask: 0xffffffff,
    }
    // write mask
    this.stencilMask = 0xffffffff
  }
  /*setClearColor(rgb) {
    if (rgb !== this.clearColor) {
      this.clearColor = rgb
      const v = VMath.rgbToFloat(rgb)
      this.gl.clearColor(v[0], v[1], v[2], 1)
    }
  }*/
  setScissorTest(enable) {
    if (this.scissorTest !== enable) {
      this.scissorTest = enable
      this.glToggle(enable, this.gl.SCISSOR_TEST)
    }
  }
  setDepthTest(enable) {
    if (this.depthTest !== enable) {
      this.depthTest = enable
      this.glToggle(enable, this.gl.DEPTH_TEST)
    }
  }
  setStencilTest(enable) {
    if (this.stencilTest !== enable) {
      this.stencilTest = enable
      this.glToggle(enable, this.gl.STENCIL_TEST)
    }
  }
  setBlend(enable) {
    if (this.blend !== enable) {
      this.blend = enable
      this.glToggle(enable, this.gl.BLEND)
    }
  }
  setDepthMask(enable) {
    if (this.depthMath !== enable) {
      this.depthMask = enable
      this.gl.depthMask(enable) // ZWRITE
    }
  }
  setCullFace(mode) {
    if (this.cullFace === mode) {
      return
    }
    if (mode === this.Cull.none) {
      this.glToggle(false, this.gl.CULL_FACE)
      this.cullFace = mode
      return
    }
    if (this.cullFace === this.Cull.none) {
      this.glToggle(true, this.gl.CULL_FACE)
    }
    this.gl.cullFace(mode)
    this.cullFace = mode
  }
  setStencilOp(fail, zfail, zpass) {
    if (this.stencilOp.fail === fail
      && this.stencilOp.zfail === zfail
      && this.stencilOp.zpass === zpass) {
      return
    }
    this.gl.stencilOp(fail, zfail, zpass)
    this.stencilOp.fail = fail
    this.stencilOp.zfail = zfail
    this.stencilOp.zpass = zpass
  }
  setStencilFunc(func, ref, mask) {
    if (this.stencilFunc.func === func
      && this.stencilFunc.ref === ref
      && this.stencilFunc.mask === mask) {
      return
    }
    // https://developer.mozilla.org/en-US/docs/Web/API/WebGLRenderingContext/stencilFuncSeparate
    // oh, if only it weren't illegal to use Separate... duh!
    // https://stackoverflow.com/a/38272458
    // glDrawElements: Front/back stencil settings do not match.
    this.gl.stencilFunc(func, ref, mask)
    this.stencilFunc.func = func
    this.stencilFunc.ref = ref
    this.stencilFunc.mask = mask
  }
  setStencilMask(mask) {
    if (this.stencilMask === mask) {
      return
    }
    this.gl.stencilMask(mask)
    this.stencilMask = mask
  }
  setDefaultStencil() {
    const { gl } = this
    this.setStencilFunc(gl.ALWAYS, 0, 0xffffffff)
    this.setStencilMask(0xffffffff)
    this.setStencilOp(gl.KEEP, gl.KEEP, gl.KEEP)
  }
  glToggle(enable, flag) {
    if (enable) {
      this.gl.enable(flag)
    } else {
      this.gl.disable(flag)
    }
  }
  viewport(x, y, w, h) {
    this.gl.viewport(x, y, w, h)
  }
  scissor(x, y, w, h) {
    this.gl.scissor(x, y, w, h)
  }
  clear() {
    const { gl } = this
    // eslint-disable-next-line no-bitwise
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT | gl.STENCIL_BUFFER_BIT)
  }
  flush() {
    this.gl.flush()
  }
  pushFramebuffer() {
    const fb = this.gl.getParameter(this.gl.FRAMEBUFFER_BINDING)
    this.stacks.framebuffer.push(fb)
  }
  pushRenderbuffer() {
    const rb = this.gl.getParameter(this.gl.RENDERBUFFER_BINDING)
    this.stacks.renderbuffer.push(rb)
  }
  popFramebuffer() {
    const fb = this.stacks.framebuffer.pop()
    if (fb) {
      this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, fb)
    } else {
      this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null)
    }
  }
  popRenderbuffer() {
    const rb = this.stacks.renderbuffer.pop()
    if (rb) {
      this.gl.bindRenderbuffer(this.gl.RENDERBUFFER, rb)
    } else {
      this.gl.bindRenderbuffer(this.gl.RENDERBUFFER, null)
    }
  }
  setTextureParameters(filter, wrap) {
    const { gl } = this
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, filter)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, filter)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, wrap)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, wrap)
  }
}
//export { GlState as default }