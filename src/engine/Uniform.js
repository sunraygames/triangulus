export default function Uniform(value) {
  // this._location = null
  this.needsUpdate = true
  // this.value = value
  this.value = value
}

Object.defineProperty(Uniform.prototype, 'value', {
  get: function() { return this._value },
  set: function(value) {
    if (this._value === value) return

    this._value = value
    this.needsUpdate = true
  },
  enumerable: true,
  configurable: true
})

Uniform.prototype.update = function(gl) {
  this.needsUpdate = false
  this.onUpdate(gl)
}

Uniform.prototype.onUpdate = function(/*gl*/) {
  // console.warn( this.constructor.name + '.onUpdate: unimplemented=', this)
}

export function UniformMatrix4() {
  Uniform.apply(this, arguments)
}
UniformMatrix4.prototype = Object.create(Uniform.prototype)
UniformMatrix4.prototype.constructor = UniformMatrix4

UniformMatrix4.prototype.onUpdate = function(gl) {
  // console.log( this.constructor.name + '.onUpdate=', this, this.value)
  gl.uniformMatrix4fv(this._location, false, this.value)
}

export function UniformVector3() {
  Uniform.apply(this, arguments)
}
UniformVector3.prototype = Object.create(Uniform.prototype)
UniformVector3.prototype.constructor = UniformVector3

UniformVector3.prototype.onUpdate = function(gl) {
  // console.log( this.constructor.name + '.onUpdate=', this, this.value)
  gl.uniform3fv(this._location, this.value)
}

export function UniformInt() {
  Uniform.apply(this, arguments)
}
UniformInt.prototype = Object.create(Uniform.prototype)
UniformInt.prototype.constructor = UniformInt

UniformInt.prototype.onUpdate = function(gl) {
  // console.log( this.constructor.name + '.onUpdate=', this, this.value)
  gl.uniform1i(this._location, this.value)
}

export function UniformFloat() {
  Uniform.apply(this, arguments)
}
UniformFloat.prototype = Object.create(Uniform.prototype)
UniformFloat.prototype.constructor = UniformFloat

UniformFloat.prototype.onUpdate = function(gl) {
  // console.log( this.constructor.name + '.onUpdate=', this, this.value)
  gl.uniform1f(this._location, this.value)
}

export function UniformTexture2D(/*value, slot*/) {
  // this.slot = slot
  Uniform.apply(this, arguments)
}
UniformTexture2D.prototype = Object.create(Uniform.prototype)
UniformTexture2D.prototype.constructor = UniformTexture2D

UniformTexture2D.prototype.onUpdate = function(gl) {
  // console.log( this.constructor.name + '.onUpdate=', this, this.value)
  gl.activeTexture(gl.TEXTURE0 + this.value.slot) // Указываем WebGL, что мы используем текстурный регистр slot
  gl.bindTexture(gl.TEXTURE_2D, this.value._texture) // Связываем текстуру с регистром slot
  gl.uniform1i(this._location, this.value.slot) // Указываем шейдеру, что мы связали текстуру с текстурным регистром slot
}