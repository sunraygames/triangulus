export let currentMaterial = null

export function setCurrentMaterial (mat) {
  currentMaterial = mat
}

export default function Material({vertexShader, fragmentShader} = {}) {
  this._vertexShader = vertexShader
  this._fragmentShader = fragmentShader

  this._attributesLocations = null
  this._shaderProgram = null
  this.shaderError = false

  this.uniforms = {}
}

Material.prototype.initShaders = function(gl) {
  const vertexShader = this.loadShader(gl, gl.VERTEX_SHADER, this._vertexShader)
  const fragmentShader = this.loadShader(gl, gl.FRAGMENT_SHADER, this._fragmentShader)

  // Create the shader program
  const shaderProgram = gl.createProgram()
  if (vertexShader) gl.attachShader(shaderProgram, vertexShader)
  if (fragmentShader) gl.attachShader(shaderProgram, fragmentShader)
  gl.linkProgram(shaderProgram)

  // If creating the shader program failed, alert
  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
    // alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram))
    console.warn('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram))
    return null
  }

  for (const name in this.uniforms) {
    const uniform = this.uniforms[name]
    uniform._location = gl.getUniformLocation(shaderProgram, name)
  }

  // console.log('initShaders=', shaderProgram, this)
  this._shaderProgram = shaderProgram
}

Material.prototype.loadShader = function(gl, type, source) {
  const shader = gl.createShader(type)

  // Send the source to the shader object
  gl.shaderSource(shader, source)

  // Compile the shader program
  gl.compileShader(shader)

  // See if it compiled successfully
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    // alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader))
    console.warn('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader))
    gl.deleteShader(shader)
    this.shaderError = true
    return null
  }

  // console.log('loadShader=' + type, source, shader)
  return shader
}

Material.prototype.activate = function(gl, camera) {
  // if (currentMaterial === this) return

  if (!this._shaderProgram && !this.shaderError) this.initShaders(gl)

  gl.useProgram(this._shaderProgram)

  if (this.uniforms.projectionMatrix) {
    this.uniforms.projectionMatrix.value = camera.projectionMatrix
    this.uniforms.projectionMatrix.needsUpdate = true
  }
  if (this.uniforms.modelViewMatrix) {
    this.uniforms.modelViewMatrix.value = camera.modelViewMatrix
    this.uniforms.modelViewMatrix.needsUpdate = true
  }

  for (const name in this.uniforms) {
    const uniform = this.uniforms[name]
    if (uniform.needsUpdate) {
      // console.log('update uniform=', name, uniform, this)
      uniform.update(gl)
    }
  }
  // gl.getExtension('OES_standard_derivatives')

  currentMaterial = this
}

Material.prototype.deactivate = function(/*gl*/) {

}

Material.prototype.initAttributesLocations = function(gl, geometry) {
  if (this._attributesLocations) return

  this._attributesLocations = {}

  if (!this._shaderProgram) return
  for (const name in geometry.buffers) {
    // const buffer = geometry.getBuffer(name)
    this._attributesLocations[name] = gl.getAttribLocation(this._shaderProgram, name)
  }
  // console.log('initAttributes=', this._attributesLocations)
}
