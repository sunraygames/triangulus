export default function Geometry() {
  this.buffers = {} // todo: array use
  this.drawRange = {offset: 0, count : 0}
}

Geometry.prototype.setBuffer = function(name, buffer) {
  this.buffers[name] = buffer
}

Geometry.prototype.getBuffer = function(name) {
  return this.buffers[name]
}

Geometry.prototype.activate = function(gl, material) {
  for (const name in this.buffers) {
    const buffer = this.buffers[name]
    if (!buffer._buffer) buffer.init(gl)
    if (buffer.updateFlag) buffer.update(gl)

    gl.bindBuffer(buffer.target, buffer._buffer)

    const location = material._attributesLocations[name]

    gl.vertexAttribPointer(location, buffer.itemSize, buffer.type, buffer.normalize, buffer.stride, buffer.offset)
    gl.enableVertexAttribArray(location)
  }
}
