export default function RenderTarget() {

}

RenderTarget.prototype.init = function(gl, textures) {
  this.textures = textures
  // Create and bind the framebuffer
  const fb = gl.createFramebuffer()
  gl.bindFramebuffer(gl.FRAMEBUFFER, fb)

  this._frameBuffer = fb

  const buffers = [/*gl.NONE, */]
  textures.forEach((texture, textureI) => {
    // attach the texture as the first color attachment
    gl.bindFramebuffer(gl.FRAMEBUFFER, fb)
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0 + textureI, texture.target, texture._texture, texture.level)
    gl.bindFramebuffer(gl.FRAMEBUFFER, null)

    buffers.push(gl.COLOR_ATTACHMENT0 + textureI)
  })

  // set drawBuffers
  gl.bindFramebuffer(gl.FRAMEBUFFER, fb)
  gl.drawBuffers(buffers)
  gl.bindFramebuffer(gl.FRAMEBUFFER, null)

  gl.bindTexture(gl.TEXTURE_2D, null)

  //gl.drawBuffers([/*gl.NONE, */gl.COLOR_ATTACHMENT0, gl.COLOR_ATTACHMENT1])
}