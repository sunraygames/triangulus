import Object3D from './Object3D'
import {DrawMode} from './Constants'

export default function Mesh(geometry, material) {
  this.geometry = geometry
  this.material = material
  this.mode = DrawMode.TRIANGLES
  Object3D.apply(this, arguments)
}
Mesh.prototype = Object.create(Object3D.prototype)
Mesh.prototype.constructor = Mesh

Mesh.prototype.render = function(gl, camera) {
  if (!this.visible) return

  // if (this.worldMatrixNeedsUpdate) this.updateWorldMatrix()

  // state.object = this
  if (this.material.uniforms.matrixWorld) {
    this.material.uniforms.matrixWorld.value = this.matrixWorld
    this.material.uniforms.matrixWorld.needsUpdate = true
    // gl.uniformMatrix4fv(this.material.uniforms.matrixWorld._location, false, this.matrixWorld)
  }

  this.material.activate(gl, camera/*state*/)

  this.material.initAttributesLocations(gl, this.geometry)

  this.geometry.activate(gl, this.material)

  gl.drawArrays(this.mode, this.geometry.drawRange.offset, this.geometry.drawRange.count)

  // this.material.deactivate(gl)
}
