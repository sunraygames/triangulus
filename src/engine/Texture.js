import {TargetType} from '@/engine/Constants'

export default function Texture(slot) {
  this.slot = slot
  // this._texture = null
}

export function Texture2D(/*target, */level, internalFormat, width, height, border, format, type, data, slot) {
  Texture.apply(this, arguments)

  this.target = TargetType.TEXTURE_2D
  this.level = level
  this.internalFormat = internalFormat
  this.width = width
  this.height = height
  this.border = border
  this.format = format
  this.type = type
  this.data = data

  this.slot = slot
}
Texture2D.prototype = Object.create(Texture.prototype)
Texture2D.prototype.constructor = Texture2D

Texture2D.prototype.init = function(gl) {
  this._texture = gl.createTexture()
  gl.bindTexture(this.target, this._texture)

  gl.texImage2D(this.target, this.level, this.internalFormat, this.width, this.height, this.border, this.format, this.type, this.data)

  // set the filtering so we don't need mips
  gl.texParameteri(this.target, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
  gl.texParameteri(this.target, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
  // gl.texParameteri(this.target, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
  gl.texParameteri(this.target, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
  gl.texParameteri(this.target, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
}
