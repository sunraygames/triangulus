import {BufferType, UsageType, DataType} from './Constants'

export default function Buffer(array, itemSize) {
  this._array = array
  this.itemSize = itemSize

  this.type = DataType.FLOAT
  this.target = BufferType.ARRAY_BUFFER
  this.usage = UsageType.STATIC_DRAW

  this.normalize = false // don't normalize
  this.stride = 0 // how many bytes to get from one set of values to the next, 0 = use type and itemSize above
  this.offset = 0 // how many bytes inside the buffer to start from

  this.count = array.length / this.itemSize

  this.updateFlag = false
  this.updateRange = {offset: 0, count: -1}
}

Buffer.prototype.init = function(gl) {
  this._buffer = gl.createBuffer()
  gl.bindBuffer(this.target, this._buffer)
  gl.bufferData(this.target, this._array, this.usage)
  // console.log('init buffer: target=' + this.target + ', usage=' + this.usage, this._buffer)
}

Buffer.prototype.update = function(gl) {
  gl.bindBuffer(this.target, this._buffer)

  const updateRange = this.updateRange

  if (updateRange.count === -1) {
    // Not using update ranges
    gl.bufferSubData(this.target, 0, this._array)
  } else {
    //if (isWebGL2) {
      gl.bufferSubData(this.target, updateRange.offset * this._array.BYTES_PER_ELEMENT, this._array, updateRange.offset, updateRange.count)
    /*} else {
      gl.bufferSubData(this.target, updateRange.offset * this._array.BYTES_PER_ELEMENT, this._array.subarray(updateRange.offset, updateRange.offset + updateRange.count))
    }*/
    updateRange.count = - 1 // reset range
  }
}
