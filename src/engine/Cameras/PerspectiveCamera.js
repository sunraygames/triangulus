import Camera from './Camera'
import {mat4} from "gl-matrix"

export default function PerspectiveCamera(fov, aspect, near, far) {
  this.fov = fov
  this.aspect = aspect
  this.near = near
  this.far = far

  this.isPerspectiveCamera = true

  Camera.apply(this, arguments)
}
PerspectiveCamera.prototype = Object.create(Camera.prototype)
PerspectiveCamera.prototype.constructor = PerspectiveCamera

PerspectiveCamera.prototype.updateProjectionMatrix = function() {
  mat4.perspective(this.projectionMatrix, this.fov, this.aspect, this.near, this.far)
}
