import Camera from './Camera'
import * as mat4 from 'gl-matrix/mat4'

export default function OrthographicCamera(left, right, top, bottom, near, far) {
  this.left = left
  this.right = right
  this.top = top
  this.bottom = bottom
  this.near = near
  this.far = far

  this.isOrthographicCamera = true

  Camera.apply(this, arguments)
}
OrthographicCamera.prototype = Object.create(Camera.prototype)
OrthographicCamera.prototype.constructor = OrthographicCamera

OrthographicCamera.prototype.updateProjectionMatrix = function() {
  mat4.ortho(this.projectionMatrix, this.left, this.right, this.bottom, this.top, this.near, this.far)
}
