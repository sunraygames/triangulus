import {mat4} from "gl-matrix"

export default function Camera() {
  this.projectionMatrix = mat4.create()
  this.modelViewMatrix = mat4.create()
}

Camera.prototype.updateProjectionMatrix = function() {}
