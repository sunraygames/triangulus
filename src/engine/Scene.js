import Object3D from './Object3D'
import {setCurrentMaterial} from './Material'

export default function Scene() {
  Object3D.apply(this, arguments)
}
Scene.prototype = Object.create(Object3D.prototype)
Scene.prototype.constructor = Scene

Scene.prototype.render = function(gl, camera) {
  gl.enable(gl.DEPTH_TEST)
  gl.depthFunc(gl.LEQUAL)

  gl.frontFace(gl.CCW)
  gl.cullFace(gl.BACK)
  gl.enable(gl.CULL_FACE)
  gl.disable(gl.BLEND)

  setCurrentMaterial(null)

  const count = this.children.length
  // if (count > 0 && this.children[0].material) this.children[0].material.activate(gl, camera/*state*/)
  for (let i = 0; i < count; i++) {
    this.children[i].render(gl, camera)
  }

  /* if (state.renderer.sortObjects) {
    this._objects.sort(this._renderOrderSort)
  }

  this._objects.forEach(object => object.render(state)) */
}
