import {initConstants} from './Constants'

export default function Renderer(options) {
  this.clearColor = options.clearColor || [1, 1, 1, 1]
  // this.clearColor = options.clearColor || [1, 0, 1, 1]

  this.init(options)
}

Renderer.prototype.init = function({canvas, webgl = 'webgl', preserveDrawingBuffer = false, antialias = false, stencil = false} = {}) {
  this._canvas = canvas

  // Initialize the GL context
  const gl = this._canvas.getContext(webgl, {preserveDrawingBuffer, antialias, stencil})

  // Only continue if WebGL is available and working
  if (gl === null) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.')
    return
  }

  /* eslint-disable no-undef */
  this._isWebGL2 = (typeof WebGL2RenderingContext !== 'undefined' && gl instanceof WebGL2RenderingContext) ||
    (typeof WebGL2ComputeRenderingContext !== 'undefined' && gl instanceof WebGL2ComputeRenderingContext)
  // console.log('isWebGL2=' + this._isWebGL2, 'gl=', gl)
  /* eslint-enable no-undef */

  initConstants(gl)

  this._gl = gl

  // gl.getExtension('OES_standard_derivatives')
  // gl.getExtension('EXT_shader_texture_lod')
}

Renderer.prototype.setSize = function(width, height) {
  /*this._size = [
    width * this._pixelRatio,
    height * this._pixelRatio
  ]*/

  if (this._canvas) {
    //this._canvas.width = this._size[0]
    //this._canvas.height = this._size[1]
    this._canvas.width = width
    this._canvas.height = height
    this._canvas.style.width = width + 'px'
    this._canvas.style.height = height + 'px'
  }

  this.setViewport(width, height)
}

Renderer.prototype.setViewport = function(width, height) {
  this._gl.viewport(0, 0, width, height)
}

Renderer.prototype.render = function(scene, camera) {
  const gl = this._gl

  // scene.typifyForRender(this._pluginsByType)

  // if (this._renderTarget) this._renderTarget.bind(gl)

  gl.clearDepth(1)
  gl.clearStencil(0)
  // if (this.autoClear) this.clear()
  gl.clearColor.apply(gl, this.clearColor)
  // gl.clearColor(gl, 0, 0, 0, 0)

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

  /*camera.updateLocalMatrix()
  camera.updateWorldMatrix()*/

  scene.render(gl, camera)

  // if (this._renderTarget) this._renderTarget.unbind(gl)
}
