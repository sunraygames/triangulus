export default `#version 300 es
  
  precision highp float;
  
  in vec2 position;
  in vec2 uv;

  //uniform mat4 modelViewMatrix;
  uniform mat4 projectionMatrix;
  //uniform mat4 matrixWorld;
  
  out vec2 vUv;

  void main() {
    vUv = uv;
    //gl_Position = projectionMatrix * modelViewMatrix * matrixWorld * vec4(position, 1.0);
    gl_Position = projectionMatrix * vec4(position, 0.0, 1.0);
  }
`