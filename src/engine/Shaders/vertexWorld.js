export default `
  precision highp float;
  attribute vec3 position;

  uniform mat4 modelViewMatrix;
  uniform mat4 projectionMatrix;
  uniform mat4 matrixWorld;

  void main() {
    gl_Position = projectionMatrix * modelViewMatrix * matrixWorld * vec4(position, 1.0);
  }
`
