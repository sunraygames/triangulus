/*export default `
  precision highp float;
  
  varying vec2 vUv;

  void main() {
    vec4 vColor = vec4(1.0, 0.0, 1.0, 1.0);
    
    float width = 0.35;
    // 'float dist = length(vUv);
    float dist = vUv.x * vUv.x + vUv.y * vUv.y;
    if (dist < 1.0) {
      if (dist < (1.0 - width)) gl_FragColor = vColor;
      else gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
    } else {
      // discard;
      // gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
    }
  }
`*/

// https://forum.unity.com/threads/antialiasing-circle-shader.432119

export default `#version 300 es
  #define IS_INT_TEX

  precision highp float;

  in vec2 vUv;
  
  uniform vec3 color;
  uniform float f2i;
  
  // out vec4 out_FragColor;
  #ifdef IS_INT_TEX
    layout(location = 0) out ivec4 gColor;
    layout(location = 1) out ivec4 gData1;
    layout(location = 2) out ivec4 gData2;
    layout(location = 3) out ivec4 gData3;
  #else
    layout(location = 0) out vec4 gColor;
    layout(location = 1) out vec4 gData1;
    layout(location = 2) out vec4 gData2;
    layout(location = 3) out vec4 gData3;
  #endif
  
  float saturate(float value) {
    return clamp(value, 0.0, 1.0);
  }
  
  //const float f2i = 255.0;
  //const float f2i = 10000.0;
  ivec4 vec4ToIvec4(vec4 value) {
    //return ivec4(value.r * f2i, value.g * f2i, value.b * f2i, value.a * f2i);
    return ivec4(value * f2i);
  }

  void main() {
    #ifdef IS_INT_TEX
      gData1 = vec4ToIvec4(vec4(vUv, 0.0, 1.0));
    #else
      gData1 = vec4(vUv, 0.0, 1.0);
    #endif
  
    //vec4 vColor = vec4(color, 1.0);
    vec4 vColor = vec4(0.0, 1.0, 0.0, 1.0);
    vec4 vColorBorder = vec4(0.0, 0.0, 0.0, 1.0);
    
    float radius = 0.5;
    float borderThickness = 0.05;

    float distance = length(vUv);
    float pwidth = length(vec2(dFdx(distance), dFdy(distance))); // fwidth is abs(ddx(x)) + abs(ddy(x)) which is a cheaper approximation of length, so instead just do length
    float alpha = smoothstep(0.0, 1.5, (radius - distance) / pwidth); // smoothstep with a 1.5 pixel falloff gets you a really smooth looking circle, where a 1 pixel linear falloff can still look minorly aliased
    // out_FragColor = vec4(vColor.rgb, vColor.a * alpha);
    float mv = smoothstep(0.0, 1.5, (radius + borderThickness - distance) / pwidth); // smoothstep with a 1.5 pixel falloff gets you a really smooth looking circle, where a 1 pixel linear falloff can still look minorly aliased
    
    #ifdef IS_INT_TEX
      gColor = vec4ToIvec4(vec4(vColor.rgb, vColor.a * alpha));
      
      gColor = vec4ToIvec4(vec4(mix(vColorBorder.rgb, vColor.rgb, alpha), mv));
      
      gData2 = vec4ToIvec4(vec4(dFdx(distance), dFdy(distance), (0.5 - distance), distance));
      gData3 = vec4ToIvec4(vec4(pwidth, fwidth(distance), (0.5 - distance) / pwidth, alpha));
    #else
      gColor = vec4(vColor.rgb, vColor.a * alpha);
      gData2 = vec4(dFdx(distance), dFdy(distance), 0.0, distance);
      gData3 = vec4(pwidth, fwidth(distance), 0.0, alpha);
    #endif
    
    //float alpha2 = smoothstep(0.0, 1.5, (0.75 - distance) / pwidth); // smoothstep with a 1.5 pixel falloff gets you a really smooth looking circle, where a 1 pixel linear falloff can still look minorly aliased
    //gColor = vec4ToIvec4(vec4(vColorBorder.rgb, vColorBorder.a * alpha2));
    
    //https://stackoverflow.com/questions/64170321/how-to-draw-a-smooth-circle-with-a-border-in-glsl
    /*float t1 = 1.0 - smoothstep(radius - borderThickness, radius, distance);
    float t2 = 1.0 - smoothstep(radius, radius + borderThickness, distance);
    gColor = vec4ToIvec4(vec4(mix(vColorBorder.rgb, vColor.rgb, t1), t2));*/
    
    //gColor = vec4ToIvec4(vec4(smoothstep(0.0, 0.05, vUv.x), 0.0, 0.0, 1.0));
    //gData2 = vec4ToIvec4(vec4(smoothstep(radius - borderThickness, radius, distance), smoothstep(radius, radius + borderThickness, distance), t1, t2));
    
    /*float distance = sqrt(dot(vUv, vUv)); // mathematically identical to your use of Pythagorean theorem, but GPU-ified, length(vUv) honestly also works and might even be faster
    float alpha = saturate((0.5 - distance) / fwidth(distance)); // fwidth is the amount of change in between both vertical and horizontal pixels, saturate clamps to 0.0 to 1.0
    // out_FragColor = vec4(vColor.rgb, vColor.a * alpha);
    gColor = vec4(vColor.rgb, vColor.a * alpha);
    gData2 = vec4(dFdx(distance), dFdy(distance), fwidth(distance), 1.0);*/
    
    // gColor = vec4(1.0, 0.1, 0.5, 1.0);
    // gColor = vec4(vUv, 0.0, 1.0);
    // out_FragColor = vec4(alpha, 0.0, 0.0, 1.0);
    
    /*float width = 0.35;
    if (distance < 1.0) {
      if (distance < (1.0 - width)) out_FragColor = vColor;
      else out_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
    } else {
      // discard;
      // out_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
    }*/
  }
`

// https://threejs.org/examples/?q=target#webgl2_multiple_rendertargets