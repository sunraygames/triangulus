/*export default `
  precision highp float;
  
  attribute vec3 position;
  attribute vec2 uv;

  uniform mat4 modelViewMatrix;
  uniform mat4 projectionMatrix;
  uniform mat4 matrixWorld;
  
  varying vec2 vUv;

  void main() {
    vUv = uv;
    gl_Position = projectionMatrix * modelViewMatrix * matrixWorld * vec4(position, 1.0);
  }
`*/

export default `#version 300 es
  
  precision highp float;
  
  in vec3 position;
  in vec2 uv;

  uniform mat4 modelViewMatrix;
  uniform mat4 projectionMatrix;
  uniform mat4 matrixWorld;
  
  out vec2 vUv;

  void main() {
    vUv = uv;
    gl_Position = projectionMatrix * modelViewMatrix * matrixWorld * vec4(position, 1.0);
  }
`