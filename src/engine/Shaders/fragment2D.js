const common = `
  #define IS_INT_TEX

  precision highp float;
  precision highp int;
  precision highp sampler2D;
  precision highp isampler2D;
  
  in vec2 vUv;
  
  #ifdef IS_FOR_RT
    layout(location = 0) out ivec4 out_FragColor;
  #else
    layout(location = 0) out vec4 out_FragColor;
  #endif
  
  #ifdef IS_INT_TEX
    uniform isampler2D tColor;
    uniform isampler2D tUv;
    uniform isampler2D tDerivatives;
    uniform isampler2D tData;
  #else
    uniform sampler2D tColor;
    uniform sampler2D tUv;
    uniform sampler2D tDerivatives;
    uniform sampler2D tData;
  #endif
  
  uniform int dataType;
  uniform float f2i;
  
  /*struct MaterialData {
    sampler2D texture;
  };
  
  uniform MaterialData textures[2] = MaterialData[2](MaterialData(tColor), MaterialData(tUv));*/
  
  //uniform sampler2D textures[2];
  
  //const float f2i = 255.0;
  //const float f2i = 10000.0;
  vec4 ivec4ToVec4(ivec4 value) {
    //return vec4(value.r * f2i, value.g * f2i, value.b * f2i, value.a * f2i);
    return vec4(float(value.r) / f2i, float(value.g) / f2i, float(value.b) / f2i, float(value.a) / f2i);
    //return vec4(value / f2i);
  }
  
  ivec4 vec4ToIvec4(vec4 value) {
    //return ivec4(value.r * f2i, value.g * f2i, value.b * f2i, value.a * f2i);
    return ivec4(value * f2i);
  }

  void main() {
    //sampler2D textures[2];
    //textures[0] = tColor;
    
    #ifdef IS_INT_TEX
      if (dataType == -1) {
        #ifdef IS_FOR_RT
          vec4 diffuse = ivec4ToVec4(texture(tColor, vUv));
          vec4 data = ivec4ToVec4(texture(tUv, vUv));
          out_FragColor = vec4ToIvec4(mix(diffuse, data, step(0.5, vUv.x)));
        #else
          vec4 diffuse = ivec4ToVec4(texture(tColor, vUv));
          vec4 data = ivec4ToVec4(texture(tUv, vUv));
          out_FragColor = mix(diffuse, data, step(0.5, vUv.x));
        #endif
      } else if (dataType == 0) {
        #ifdef IS_FOR_RT
          out_FragColor = texture(tColor, vUv);
        #else
          out_FragColor = ivec4ToVec4(texture(tColor, vUv));
        #endif
      } else if (dataType == 1) {
        #ifdef IS_FOR_RT
          out_FragColor = texture(tUv, vUv);
        #else
          out_FragColor = ivec4ToVec4(texture(tUv, vUv));
        #endif
      } else if (dataType == 2) {
        #ifdef IS_FOR_RT
          out_FragColor = texture(tDerivatives, vUv);
        #else
          out_FragColor = ivec4ToVec4(texture(tDerivatives, vUv));
        #endif
      } else if (dataType == 3) {
        #ifdef IS_FOR_RT
          out_FragColor = texture(tData, vUv);
        #else
          out_FragColor = ivec4ToVec4(texture(tData, vUv));
        #endif
      }
    #else
      if (dataType == -1) {
        out_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
        // out_FragColor = texture(tColor, vUv);
        //out_FragColor = texture(tUv, vUv);
        
        //vec3 diffuse = vec3(1.0, 0.0, 0.0);
        //vec3 diffuse = texture(tColor, vUv).rgb;
        //vec3 diffuse = texture(tColor, vec2(0.5, 0.5)).rgb;
        //vec3 data = texture(tUv, vUv).rgb;
        vec4 diffuse = texture(tColor, vUv);
        vec4 data = texture(tUv, vUv);
        //vec4 data = vec4(1.0, 0.0, 0.0, 1.0);
    
        //out_FragColor.rgb = mix(diffuse, data, step(0.5, vUv.x));
        out_FragColor = mix(diffuse, data, step(0.5, vUv.x));
        //out_FragColor.rgb = diffuse;
        //out_FragColor.a = 1.0;
        
        //out_FragColor.rg = vUv;
        
        //out_FragColor.g = diffuse.b;
      } else if (dataType == 0) {
        out_FragColor = texture(tColor, vUv);
      } else if (dataType == 1) {
        out_FragColor = texture(tUv, vUv);
      } else if (dataType == 2) {
        out_FragColor = texture(tDerivatives, vUv);
      } else if (dataType == 3) {
        out_FragColor = texture(tData, vUv);
      }
    #endif
  }
`

export function getShaderFragment2D(forRt) {
  const shader = `#version 300 es\n` + (forRt ? '#define IS_FOR_RT ' + forRt + '\n' : '') + common
  // console.log('getShaderFragment2D=', shader)
  return shader
}
